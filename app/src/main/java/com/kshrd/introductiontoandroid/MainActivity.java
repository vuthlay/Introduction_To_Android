package com.kshrd.introductiontoandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    String str = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.print("On Create");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.print("On Start");
        str = "Hello";
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.print("On Resume");
        Logger.print(str);
        str = "Bonjour";
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.print("On Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.print("On Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.print("On Destroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Logger.print("On Restart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.print("On Save Instance State");
        outState.putString("str", str);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Logger.print("On Restore Instance State");
        str = savedInstanceState.getString("str");
    }
}
